const encryptionWithKeyAlgo = require('./encryptionWithKey');


function encryptText(key, text) {
    return encryptionWithKeyAlgo.encryptText(Buffer.from(key, 'base64'), text)
}

function decrypt(key, text) {
    return encryptionWithKeyAlgo.decrypt(Buffer.from(key, 'base64'), text)
}

function generateKey() {
    return encryptionWithKeyAlgo.generateKey().toString('base64')
}

module.exports = {
    generateKey,
    encryptText,
    decrypt,
};

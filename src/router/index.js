import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/dashboard/Home.vue'
import About from '../views/auth/Login.vue'
import Register from '../views/auth/Register.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home
	},
	{
		path: '/about',
		name: 'About',
		component: About
	},

	/* login routes  */
	{
		path: '/login',
		name: 'Login',
		component: About
	},
	{
		path: '/register',
		name: 'Register',
		component: Register
	}
]

const router = new VueRouter({
	routes
})

export default router

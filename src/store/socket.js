const state = {
	errorMessage: '',
}
const getters = {}
const mutations = {
	socket_connect(state) {
		console.log('socket connected from vuex')
	},

	socket_disconnect(state) {
		console.log('socket disconnected from vuex')
	},

	socket_connect_error(state,error) {
		console.log('socket connect_error from vuex')
		state.errorMessage = error
	},
	
	socket_testServer(state, message) {
		console.log(message)
	},

	socket_exceptions(state, error) {
		console.log('%c' + error.code +': ' + error.message,'background: #EB3941; color: #fff')
		state.errorMessage = error.message
	},

	updateErrorMessage: (state,message) => state.errorMessage = message
}
const actions = {}

export default {
	state,
	getters,
	mutations,
	actions
}
